/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *Esta clase modela un número entero más grande que un long
 * @author Israel Bulla
 */
public class EnteroGrande {
    private int vector[];

    public EnteroGrande() {
    }

    public int[] getVector() {
        return vector;
    }

    public void setVector(int[] vector) {
        this.vector = vector;
    }
    
    public EnteroGrande(int n){
        if(n<=0){
            throw new RuntimeException("Error, no se pueden crear vectores con tamaño negativo o vacio");             
        }
        this.vector = new int[n];
        for (int i = 0; i < vector.length; i++) {
            vector[i] = i;          
        }
    }

    @Override
    public String toString() 
{        String mensaje = "";
        for (int i : vector) {
            mensaje += i + "\t";
        }
        return mensaje;
    }
    
    
}
